/*require('dotenv').config();*/ // for virtual env
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
/*const morgan = require('morgan');
*/
const app = express();

const port = 4008;

//External Routes
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');
const cartRoutes = require('./routes/cartRoutes');
const sellerRoutes = require('./routes/sellerRoutes');


// Database Connection

mongoose.connect("mongodb+srv://admin:admin123@zuittbootcamp.x9lrsnv.mongodb.net/Backend?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).catch(err => console.log(err));


mongoose.connection.once('open', () =>{
	console.log("Database connection successful!");
	app.listen(port, () => console.log(`API is now online on port ${port}`));

});

// CORS stands for Cross-Origin Resources Sharing. 

//middlewares
/*app.use(morgan('dev'))*/;//tagging middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//Hello World
/*app.get('/', (req,res) => res.send ({response: "Hello World"}))
*/

// Main Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/api/cart", cartRoutes);
app.use("/sellers", sellerRoutes);

