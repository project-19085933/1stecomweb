const express = require('express');
const router = express.Router();

//Controllers
const auth = require('../auth')
const productController = require('../controllers/productController');


//Add Products as admin
router.post('/add-product-admin', auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.addProductAsAdmin(data).then(resultFromController => res.send(resultFromController));
});

//Add products as seller
router.post('/add-product-seller', auth.verify, (req, res) => {
    const data = {
        product: req.body,
        userId: auth.decode(req.headers.authorization).id
    };
    console.log("req.body.productName: " + req.body.productName)
    productController.addProductAsSeller(data).then(resultFromController => res.send(resultFromController));
});

//Retrieve All Products
router.get('/all', (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

//Retrieve all Active Products
router.get('/active', (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

//Search Product
router.get('/search', (req, res) => {
	const data = {
		searchQuery: req.query.searchQuery
	}
	console.log(data.searchQuery)
	productController.searchProducts(data).then(resultFromController => res.send(resultFromController));
})

//Search Category
router.get('/category/:category', (req, res) => {
    const data = {
        searchQuery: req.params.category
    }
    console.log(data.searchQuery)
    productController.searchCategory(data).then(resultFromController => res.send(resultFromController));
})

//Retrieve a single product
router.get('/:productId/details', (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

//Update product Informantion
router.put('/:productId/update', auth.verify, (req, res) => {
	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});

//Archieve Product

router.put('/:productId/archive', auth.verify, (req, res) => {
    const data = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };

    productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

//Activate Product
router.put('/:productId/activate', auth.verify, (req, res) => {
    const data = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };

    productController.activateProduct(data).then(resultFromController => res.send(resultFromController));
});


//Add product review
router.post('/:productId/add-review', auth.verify, (req, res) => {
    const data = {
        productId: req.params.productId,
        userId: auth.decode(req.headers.authorization).id,
        rating: req.body.rating,
        comment: req.body.comment
    }

    productController.addReview(data).then(resultFromController => res.send(resultFromController));
});




module.exports = router;